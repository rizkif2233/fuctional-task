const by = function (name, minor, isAscending=true) {
    const reverseMutliplier = isAscending ? 1 : -1;
    return function (o, p) {
        let a, b;
        let result;
        if (o && p && typeof o === "object" && typeof p === "object") {
            a = o[name];
            b = p[name];
            if (a === b) {
                return typeof minor === 'function' ? minor(o, p) : 0;
            }
            if (typeof a === typeof b) {
                result = a < b ? -1 : 1;
            } else {
                result = typeof a < typeof b ? -1 : 1;
            }
            return result * reverseMutliplier;
        } else {
            throw {
                name: "Error",
                message: "Expected an object when sorting by " + name
            };
        }
    };
};

let s = [
    {name: 'Xiaomi Redmi 5A',   category: 'Smartphone',price:1199000},
    {name: 'Macbook Air',   category: 'Laptop',price:13775000},
    {name: 'Samsung Galaxy J7',   category: 'Smartphone',price:3549000},
    {name: 'DELL XPS 13', category: 'Laptop',price:26799000},
    {name: 'Xiaomi Mi 6', category: 'Smartphone',price:5399000},
    {name: 'LG V30 Plus', category: 'Smartphone',price:10499000}
];


s.sort(by("first", by("last")));    
console.log("Result ", s);