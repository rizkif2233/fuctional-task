//1*Lower
var lower = 'MAKAN';

console.log(lower.toLowerCase());

//2*Uppercase
var upper = 'malam';

console.log(upper.toUpperCase());

//3*Capitalize
function capital_letter(str) 
{
    str = str.split(" ");

    for (var i = 0, x = str.length; i < x; i++) {
        str[i] = str[i][0].toUpperCase() + str[i].substr(1);
    }

    return str.join(" ");
}

console.log(capital_letter("saya ingin makan."));


//4*Reverse
function reverseWord (sentence) {
    return sentence.split(' ').map(function(word) {
      return word.split('').reverse().join('');
    }).join(' ');
  }
  
  console.log(reverseWord("kasur"));

//5*Count

//6*Count
const CHAR = '*'
const BANNED = ['dolor', 'elit', 'quis', 'nisi','fugiat','proident','laborum']
const sentence = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'

const censor = (sentence) =>
  sentence.split(' ').reduce((acc, word) =>
    acc + ' ' + (BANNED.includes(word) ? CHAR.repeat(word.length) : word), '')

const censored = censor(sentence).trim()

console.log(censored)


//7*Count
function search() { 
    var string = "Aku ingin begini Aku ingin begitu Ingin ini itu banyak sekali Semua semua semua Dapat dikabulkan Dapat dikabulkan Dengan kantong ajaib Aku ingin terbang bebas Di angkasa Hei… baling baling bambu La... la... la... Aku sayang sekali Doraemon La... la... la... Aku sayang sekali";

    var want = string.split("ingin").length; //No of words
    var dapat = string.split("Dapat").length; //No of words
    var me = string.split("Aku").length; //No of words

    console.log('Aku',me);
    console.log('ingin',want);
    console.log('Dapat',dapat);

}
search();

//8*Count
  function wordCount(str){
    return str.split('').length;
  }
  console.log(wordCount('lorem ipsum'))

//9*CountWords
  function wordCount(str){
    return str.split(' ').length;
  }
  console.log(wordCount('lorem ipsum'))
  
//10*Count

//11*Count